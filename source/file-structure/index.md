---
title: File structure
---

## File structure

Exo follows a specific file structture.

You can see it below.

### Files tree

```
├───config
├───shopify
│   ├───assets
│   ├───config
│   ├───layout
│   ├───locales
│   ├───sections
│   ├───snippets
│   ├───templates
│   │   └───customers
│   └───config.yml
├───src
│   ├───ajax
│   ├───assets
│   │   ├───fonts
│   │   ├───images
│   │   │   ├───sprite
│   │   │   └───temp
│   │   ├───js
│   │   │   ├───base
│   │   │   └───sliders
│   │   ├───scss
│   │   │   ├───1-abstracts
│   │   │   ├───2-defaults
│   │   │   ├───3-base
│   │   │   ├───4-layout
│   │   │   └───5-components
│   │   └───vendor
│   └───partials
├───package.json
├───README.md
└───settings.json
```

### 1. `config` folder

This folder includes all the configurable files.
The `config` folder should be present in your project in order for the setup to work properly.

### 2. `shopify` folder

The folder includes the final files which are uploaded to the Shopify store.
You can also zip this folder and upload it as a new theme if needed.

### 3. `src` folder

The folder include all the source files.

From here you can update the styles add scripts of the Shopify theme.

**Styles**

The main style file is in the `src/assets/scss` folder and is called `style.scss`.
Exo supports all features of the SCSS syntax (including imports, nesting, mixins, loops, conditions and so on).

The compiled file is called `bundle.css` and is uploaded to the `/shopify/assets` folder.

**JavaScript**

The main JavaScript file is in the `src/assets/js` folder and is called `main.js`.
Exo supports all features of ES6 via Babel.

You can include scripts using npm/yarn.

The compiled file is called `bundle.css` and is uploaded to the `/shopify/assets` folder.
