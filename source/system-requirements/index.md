---
title: System Requirements
date: 2019-11-11 16:05:24
---

# System Requirements

## Tips
Use bash terminal (default terminal on OSX and Linux, [GitBash](http://git-scm.com/downloads) on Windows).

## Dependencies
1. Latest version of [NodeJS](http://nodejs.org/) (min v6.0.0)
2. Latest version of any of the following package managers

- [NPM](https://www.npmjs.com/) (min v5.3.0)
- [Yarn](https://yarnpkg.com/) (min v0.20.4)

3. Latest version of [Shopify Theme Kit](https://shopify.github.io/themekit/)
