---
title: Connect your store
date: 2019-11-11 16:05:24
---

## Shopify Development

First you need to set the configuration file.

You must run the following command in the SHOPIFY folder:

```
theme configure -p PRIVATE_APP_PASSWORD -s STORE_URL_WITHOUT_HTTPS -t THEME_ID
```

Example:

```
theme configure -p a2d4e531e781abbb525ef5194b95ec68 -s example.myshopify.com -t 570949641

```

After that you can use the following command in the ROOT directory.

```
npm start
```

or

```
yarn start
```
