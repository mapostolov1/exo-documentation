---
title: Create a new project
date: 2019-11-11 16:05:24
---

## Install
In the root directory of the project run:

```
npm install
```

or

```
yarn
```

If for some reason, NPM/Yarn throws errors and does not want to install the dependencies, please see https://goo.gl/iSz4w8.

TL;DR

Run

```
npm cache clean
```

If that does not fix the issue, manually remove everything in the `~/AppData/Roaming/npm-cache` folder.

MAC/Linux users should try and find another way to delete this folder's contents because they do not have access to this folder by default.

Then run the install script again.

## MAC OSX Setup

If you are using OSX, you need to run the following (only once):

1. `brew update`
2. `brew install libtool automake autoconf nasm`
3. `brew reinstall libpng`

## Linux setup

If you are using linux environment, you need to run the following (only once):

1. `sudo apt-get install libtool automake autoconf nasm`

## Development

After installing your theme in the `shopify` folder, follow the steps below:

First run the following command which will clean the `assets` folder in the Shopify theme:

```
npm run clean
```

or

```
yarn run clean
```

If you want to work only on the front-end part, run:

```
npm run html
```

or

```
yarn run html
```
