---
title: About EXO
---

# What's EXO

EXO is a command line tool for developing Shopify Themes. It is designed to assist your development workflow and speed up the process of developing, testing, and deploying themes to Shopify.

## Why you should choose Exo:

* Dependency management via Webpack
* ES6+ support via Babel
* Browsers support (Chrome, Safari, FireFox, Internet Explorer 11, Edge)
* Setup support
* Fast development
* Predefined styles for all default pages (accounts, cart, products, collections)
* Use regular SCSS (including mixins, imports, nesting and so on)
* Moduled SCSS
* Sourcemaps
* Multi-environments development
* Deployment process (including minifying of the compiled CSS and JS)
* Split vendor and app code (both JS and SCSS)
* Remove unused css rules on production
* Shopify Plus in mind (Checkout layouts) //////////////
* Based on Gulp 4
* Live reloading with Browser Sync
* Easy dev workflow from static markup to Shopify
* Easily extend Webpack to add Applo w/ GraphQL, Vue, React, etc
* Automated Sprite generator
* Open source
