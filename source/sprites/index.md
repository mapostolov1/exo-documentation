---
title: Sprites
---

# Sprites

Almost every website uses some kind of icons.<br />
For some they are fonts, others use SVGs.

With Exo, you can very easily create the styles for PNG icons.<br />
For this, we're using `postcss-sprites`.

It gets all the images from the `/src/assets/images/sprite/` folder and creates style based on these files.

For example, if you need a cart icon, you can simply add a PNG file called `ico-cart` to the `sprite` folder.<br />
The generated CSS will use the sizes of the actual image you've included.

In the HTML, you can call the icon by only using its class (the same as the file name) - `<i class="ico-cart"></i>`.

You can also upload retina icons by adding `@2x` after the file name. For example - `ico-cart@2x.png`.

Another feature is that you can add hover effects as well.

Use this syntax for the file name:

```
.ico-cart_hover.png - Hover state
```

This icon will be used when the actual `ico-cart` (or a parent `a` tag) is hovered.

Here are all the possible states:

```
.ico-close.png - Normal icon
.ico-close_hover.png - Hover state
.ico-close@2x.png - Retina icon
.ico-close_hover@2x.png - Retina hover icon
```

test
