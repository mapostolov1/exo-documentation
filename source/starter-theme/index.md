---
title: Starter theme
---

# Starter theme

We have a theme with basic styles for the default Shopify pages.<br />
The theme is created in a way so it can inherit your base font styles.

----

Here's preview link where you can check the basic styles for the pages:

* Homepage - https://exo-theme.myshopify.com/
* Collection - https://exo-theme.myshopify.com/collections/all
* Product - https://exo-theme.myshopify.com/collections/all/products/test-product
* Cart - https://exo-theme.myshopify.com/cart
* Account pages - https://exo-theme.myshopify.com/account/
* Blog - https://exo-theme.myshopify.com/blogs/news
* Article - https://exo-theme.myshopify.com/blogs/news/blog-article
