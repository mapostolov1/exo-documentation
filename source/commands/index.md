---
title: Commands
date: 2019-11-11 16:05:24
---

## Start the project

First you need to install all the dependancies.

```
npm install
```

or

```
yarn
```

---

After the installation is complete, just run

```
npm run start
```

or

```
yarn start
```

If you want to launch the development without firstly downloading the theme, you can use

```
npm run gulp
```

or

```
yarn gulp
```

### Why do we do the initial download?
We've been in a situation when updates have been made to the theme through the Shopify's panel.
This way you will have all the latest files on your local machine.

After the download is complete, we're triggering a repo commit which will keep these updates tracked.

As you know, Shopify doesn't keep track on the updates of the `/assets` folder.
Using our approach guarantees you that none updates will be overwritten.

## Downloading Theme

To download a theme you use the following command

```
npm run download
```

or

```
yarn download
```

## Build
To build the project, run:

```
npm run deploy
```

or

```
yarn run deploy
```

## Production
To build the project for production environment (e.g. minimize bundles css and js files and optimize images), run:

```
npm run deploy-prod
```

or

```
yarn run deploy-prod
```
